﻿======================================================
Solitaire

Autoři: 
o	Martin Pitřík <xpitri00@stud.fit.vutbr.cz>
o	Adam Šulc <xsulca00@stud.fit.vutbr.cz>
======================================================

======================================================
Kompilace a spuštění
======================================================
Pro překlad zadejte make, pro spuštění make run. 

======================================================
O programu
======================================================
Program implementuje karetní hru Solitaire. Cílem hry 
je seřadit karty jednotlivých barev od esa do krále. 

Za každé umístění karty je připsáno 15 bodů, za přesun
karty z balíčku je připsáno 5 bodů a za otočení karty 
rovněž 5 bodů. 100 bodů se ubírá za otočení balíčku, 
2 body za použití kroku zpět. 

Program umožňuje rozehrát až čtyři hry současně. 

Rozehranou hru lze kdykoliv uložit, případně lze načíst
již uloženou hru.
