/**
* @file   Extensions.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __EXTENSIONS_
#define __EXTENSIONS_

#include <string>
#include <vector>

using namespace std;

namespace extensions
{
	/**
     * @brief Input string is splitted by delimeter.
     * @param String.
     * @param Delimeter.
     * @retval Splitted string.
     */
    vector<string> split(string text, const string& delimeter);
}

#endif
