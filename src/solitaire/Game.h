/**
* @file   Game.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __GAME__
#define __GAME__

#include <vector>
#include <algorithm>
#include <vector>
#include <sstream>
#include <fstream>

#include "Card.h"
#include "CardVectorNames.h"
#include "HelpResult.h"
#include "Extensions.h"

using namespace std;

namespace core
{
    /**
     * @brief The Game class
     */
    class Game
    {
    public:
        /**
         * @brief         Create game.
         */
        Game();

        /**
         * @brief         Release memory.
         */
        ~Game();

        /**
         * @brief Get all cards.
         * @return All cards.
         */
        vector<Card> getCards();

        /**
         * @brief Get all cards for moving with selected card.
         * @param Selected card.
         * @param Source card vector name.
         * @return Cards.
         */
        vector<Card> getCardsForMoving(const Card& sourceCard,
                                       CardVectorNames src);

        /**
         * @brief Card move executor.
         * @param Selected card.
         * @param Source card vector name.
         * @param Destination card vector name.
         */
        void cardMoveExec(const Card& sourceCard,
                          CardVectorNames src,
                          CardVectorNames dst);

        /**
         * @brief Card move logic.
         * @param Selected card.
         * @param Source card vector name.
         * @param Destination card vector name.
         * @return If it is true, move is possible.
         */
        bool cardMoveLogic(const Card& sourceCard,
                           CardVectorNames src,
                           CardVectorNames dst);

        /**
         * @brief Get Cards from CardVector.
         * @param CardVector name.
         * @return Cards.
         */
        vector<Card> getCardVector(CardVectorNames name);

        /**
         * @brief Switch to next card in package.
         */
        void nextCardInWaste();

        /**
         * @brief Check if game is completed.
         * @return It is true if game is completed, false if not.
         */
        bool isGameCompleted() const;

        /**
         * @brief Get help.
         * @return Possible move.
         */
        HelpResult help();

        /**
         * @brief Undo.
         */
        void undo();

        /**
         * @brief Save game to file.
         * @param Name of file.
         */
        void saveGame(const string& filename) const;

        /**
         * @brief Load game from file.
         * @param Name of file.
         */
        void openGame(const string& filename);

        /**
         * @brief Start new game.
         */
        void newGame();

        /**
         * @brief         Get player's score.
         * @retval        Score.
         */
        int getScore() const;

        int getTotalGamesWon() const;

        int getTotalGames() const;

        int getMovesCounter() const;

    private:
        vector<Card*> _allCards;
        vector<vector<vector<Card>>> _prevGameStates;
        vector<Card*> _waste;
        vector<Card*> _usedWaste;
        vector<Card*> _foundation1;
        vector<Card*> _foundation2;
        vector<Card*> _foundation3;
        vector<Card*> _foundation4;
        vector<Card*> _tableau1;
        vector<Card*> _tableau2;
        vector<Card*> _tableau3;
        vector<Card*> _tableau4;
        vector<Card*> _tableau5;
        vector<Card*> _tableau6;
        vector<Card*> _tableau7;
        int _score;
        int _movesCounter;
        int _totalGames;
        int _totalGamesWon;

        vector<Card*> getCards(unsigned int offset, unsigned int count);
        vector<Card*> getCards(unsigned int offset);
        vector<Card*>* getCardVectorR(CardVectorNames name);
        void initCards();
        void initGame();
        unsigned int findCard(vector<Card*>* cards, const Card& findCard);
        Card* getCardFrom(CardVectorNames src);
        Card* getCardFrom(unsigned int cardIndex, CardVectorNames src);
        vector<Card*> getCardsFrom(unsigned int offset, CardVectorNames src);
        static vector<Card> getVectorState(const vector<Card*>& cards);
        static vector<Card*> getVectorState(const vector<Card>& state);
        static string serializeVector(const vector<Card*>& cards);
        vector<Card*> deserializeVector(const string& line) const;
        static string serializeCard(Card* card);
        Card* deserializeCard(const string& line) const;
        vector<vector<Card>> getState();
        void setState(const vector<vector<Card>>& state);
        void addScore(int points);
        void clearAll();
    };
}

#endif
