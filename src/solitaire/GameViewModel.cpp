/**
* @file   GameViewModel.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "GameViewModel.h"

GameViewModel::GameViewModel(QGraphicsScene* scene,
                             Game* game,
                             unsigned int gameNumber,
                             unsigned int xOffset,
                             unsigned int yOffset)
{
    _scene = scene;
    _game = game;
    _xOffset = xOffset;
    _yOffset = yOffset;
    _gameNumber = gameNumber;

    init();
    addItems();
    drawBoard();
}

GameViewModel::~GameViewModel()
{
    clearHelp();
    removeItems();
}

void GameViewModel::setGameBoardOffset(unsigned int x, unsigned int y)
{
    _xOffset = x;
    _yOffset = y;

    removeItems();
    init();
    addItems();
    drawBoard();
}

void GameViewModel::cardSelectedSlot(CardViewModel* card,
                                     unsigned int x,
                                     unsigned int y)
{
    clearHelp();

    try
    {
        auto vectorName = getVectorName(x, y);
        _movedCards = _game->getCardsForMoving(
            card->getCoreCard(),
            vectorName);
        _sourceName = vectorName;
    }
    catch (...) {}
}

void GameViewModel::cardMovedSlot(CardViewModel*,
                                  unsigned int x,
                                  unsigned int y)
{
    drawCardVector(_movedCards, QPoint(x, y), 20, 100000000);
}

void GameViewModel::cardReleasedSlot(CardViewModel* card,
                                     unsigned int x,
                                     unsigned int y)
{
    _movedCards.clear();

    try
    {
        auto vectorName = getVectorName(x, y);

        _destinationName = vectorName;
        _game->cardMoveExec(
            card->getCoreCard(),
            _sourceName,
            _destinationName);
    }
    catch (...) {}

    drawBoard();
    printGameIsCompleted();
}

void GameViewModel::cardRClickedSlot(CardViewModel* card,
                                     unsigned int x,
                                     unsigned int y)
{
    clearHelp();

    try
    {
        auto srcVectorName = getVectorName(x, y);

        _game->cardMoveExec(
            card->getCoreCard(),
            srcVectorName,
            Foundation1);
        _game->cardMoveExec(
            card->getCoreCard(),
            srcVectorName,
            Foundation2);
        _game->cardMoveExec(
            card->getCoreCard(),
            srcVectorName,
            Foundation3);
        _game->cardMoveExec(
            card->getCoreCard(),
            srcVectorName,
            Foundation4);
    }
    catch (...) {}

    drawBoard();
    printGameIsCompleted();
}

void GameViewModel::nextCardInWasteSlot()
{
    _game->nextCardInWaste();

    clearHelp();
    drawBoard();
}

void GameViewModel::openGameSlot()
{
    auto fileName = QFileDialog::getOpenFileName();
    if (!fileName.isEmpty())
    {
        _game->openGame(fileName.toStdString());

        for (CardViewModel* card : _cards)
        {
            card->hide();
        }
    }

    drawBoard();
}

void GameViewModel::saveGameSlot()
{
    auto fileName = QFileDialog::getSaveFileName();
    if (!fileName.isEmpty())
    {
        _game->saveGame(fileName.toStdString());
    }
}

void GameViewModel::undoSlot()
{
    _game->undo();

    for (CardViewModel* card : _cards)
    {
        card->hide();
    }

    drawBoard();
}

void GameViewModel::newGameSlot()
{
    _game->newGame();

    for (CardViewModel* card : _cards)
    {
        card->hide();
    }

    drawBoard();
}

void GameViewModel::helpSlot()
{
    clearHelp();

    auto possibleMove = _game->help();

    if (possibleMove.source == Waste || possibleMove.destination == Waste)
    {
        drawHelp(_wastePosition);
    }
    if (possibleMove.source == Foundation1 ||
        possibleMove.destination == Foundation1)
    {
        drawHelp(_foundation1Position);
    }
    if (possibleMove.source == Foundation2 ||
        possibleMove.destination == Foundation2)
    {
        drawHelp(_foundation2Position);
    }
    if (possibleMove.source == Foundation3 ||
        possibleMove.destination == Foundation3)
    {
        drawHelp(_foundation3Position);
    }
    if (possibleMove.source == Foundation4 ||
        possibleMove.destination == Foundation4)
    {
        drawHelp(_foundation4Position);
    }
    if (possibleMove.source == Tableau1 || possibleMove.destination == Tableau1)
    {
        drawHelp(_tableau1Position);
    }
    if (possibleMove.source == Tableau2 || possibleMove.destination == Tableau2)
    {
        drawHelp(_tableau2Position);
    }
    if (possibleMove.source == Tableau3 || possibleMove.destination == Tableau3)
    {
        drawHelp(_tableau3Position);
    }
    if (possibleMove.source == Tableau4 || possibleMove.destination == Tableau4)
    {
        drawHelp(_tableau4Position);
    }
    if (possibleMove.source == Tableau5 || possibleMove.destination == Tableau5)
    {
        drawHelp(_tableau5Position);
    }
    if (possibleMove.source == Tableau6 || possibleMove.destination == Tableau6)
    {
        drawHelp(_tableau6Position);
    }
    if (possibleMove.source == Tableau7 || possibleMove.destination == Tableau7)
    {
        drawHelp(_tableau7Position);
    }
}

void GameViewModel::init()
{
    _wastePosition = QPoint(_xOffset + 100, _yOffset + 60);
    _wasteButtonPosition = QPoint(_xOffset, _yOffset + 60);
    _tableau1Position = QPoint(_xOffset, _yOffset + 210);
    _tableau2Position = QPoint(_xOffset + 100, _yOffset + 210);
    _tableau3Position = QPoint(_xOffset + 200, _yOffset + 210);
    _tableau4Position = QPoint(_xOffset + 300, _yOffset + 210);
    _tableau5Position = QPoint(_xOffset + 400, _yOffset + 210);
    _tableau6Position = QPoint(_xOffset + 500, _yOffset + 210);
    _tableau7Position = QPoint(_xOffset + 600, _yOffset + 210);
    _foundation1Position = QPoint(_xOffset + 300, _yOffset + 60);
    _foundation2Position = QPoint(_xOffset + 400, _yOffset + 60);
    _foundation3Position = QPoint(_xOffset + 500, _yOffset + 60);
    _foundation4Position = QPoint(_xOffset + 600, _yOffset + 60);
    _scorePosition = QPoint(_xOffset + 600, _yOffset + 5);
    _movesCounterPosition = QPoint(_xOffset + 600, _yOffset + 20);
    _totalGamesPosition = QPoint(_xOffset + 500, _yOffset + 5);
    _totalGamesWonPosition = QPoint(_xOffset + 500, _yOffset + 20);

    _lblScore = new QLabel();
    _lblMovesCounter = new QLabel();
    _lblTotalGames = new QLabel();
    _lblTotalGamesWon = new QLabel();

    _lblScore->setGeometry((QRect(_scorePosition, QSize(70, 15))));
    _lblMovesCounter->
            setGeometry((QRect(_movesCounterPosition, QSize(70, 15))));
    _lblTotalGames->setGeometry((QRect(_totalGamesPosition, QSize(70, 15))));
    _lblTotalGamesWon->
            setGeometry((QRect(_totalGamesWonPosition, QSize(70, 15))));

    _lblScore->setStyleSheet(_lblStyleSheet);
    _lblMovesCounter->setStyleSheet(_lblStyleSheet);
    _lblTotalGames->setStyleSheet(_lblStyleSheet);
    _lblTotalGamesWon->setStyleSheet(_lblStyleSheet);

    QPushButton* btnNew = new QPushButton("Nová hra");
    QPushButton* btnOpen = new QPushButton("Otevřít hru");
    QPushButton* btnSave = new QPushButton("Uložit hru");
    QPushButton* btnUndo = new QPushButton("Vrátit zpět");
    QPushButton* btnHelp = new QPushButton("Nápověda");
    QPushButton* btnNextCard = new QPushButton("Táhnout");

    btnNew->setGeometry((QRect(QPoint(_xOffset + 0,
                                      _yOffset + 0),
                               QSize(80, 50))));
    btnOpen->setGeometry((QRect(QPoint(_xOffset + 80,
                                       _yOffset + 0),
                                QSize(80, 50))));
    btnSave->setGeometry((QRect(QPoint(_xOffset + 160,
                                       _yOffset + 0),
                                QSize(80, 50))));
    btnUndo->setGeometry((QRect(QPoint(_xOffset + 240,
                                       _yOffset + 0),
                                QSize(80, 50))));
    btnHelp->setGeometry((QRect(QPoint(_xOffset + 320,
                                       _yOffset + 0),
                                QSize(80, 50))));
    btnNextCard->setGeometry((QRect(QPoint(_wasteButtonPosition.x(),
                                           _wasteButtonPosition.y()),
                                    QSize(88, 128))));

    QObject::connect(btnOpen,
                     SIGNAL(released()),
                     this,
                     SLOT(openGameSlot()));
    QObject::connect(btnSave,
                     SIGNAL(released()),
                     this,
                     SLOT(saveGameSlot()));
    QObject::connect(btnNextCard,
                     SIGNAL(released()),
                     this,
                     SLOT(nextCardInWasteSlot()));
    QObject::connect(btnUndo,
                     SIGNAL(released()),
                     this,
                     SLOT(undoSlot()));
    QObject::connect(btnNew,
                     SIGNAL(released()),
                     this,
                     SLOT(newGameSlot()));
    QObject::connect(btnHelp,
                     SIGNAL(released()),
                     this,
                     SLOT(helpSlot()));

    _buttons.push_back(btnNew);
    _buttons.push_back(btnOpen);
    _buttons.push_back(btnSave);
    _buttons.push_back(btnUndo);
    _buttons.push_back(btnHelp);
    _buttons.push_back(btnNextCard);
}

void GameViewModel::addItems()
{
    _cards.clear();

    auto coreCards = _game->getCards();
    for (auto coreCard : coreCards)
    {
        auto viewModel = new CardViewModel(coreCard);

        _cards.push_back(viewModel);
        viewModel->hide();
        _scene->addItem(viewModel);

        QObject::connect(viewModel,
                         SIGNAL(cardReleased(CardViewModel*, uint, uint)),
                         this,
                         SLOT(cardReleasedSlot(CardViewModel*, uint, uint)));
        QObject::connect(viewModel,
                         SIGNAL(cardMoved(CardViewModel*, uint, uint)),
                         this,
                         SLOT(cardMovedSlot(CardViewModel*, uint, uint)));
        QObject::connect(viewModel,
                         SIGNAL(cardSelected(CardViewModel*, uint, uint)),
                         this,
                         SLOT(cardSelectedSlot(CardViewModel*, uint, uint)));
        QObject::connect(viewModel,
                         SIGNAL(cardRightClicked(CardViewModel*,uint,uint)),
                         this,
                         SLOT(cardRClickedSlot(CardViewModel*,uint,uint)));
    }

    for (auto button : _buttons)
    {
        _scene->addWidget(button);
    }

    _scene->addWidget(_lblScore);
    _scene->addWidget(_lblMovesCounter);
    _scene->addWidget(_lblTotalGames);
    _scene->addWidget(_lblTotalGamesWon);

    drawEmptyVector(_wastePosition);
    drawEmptyVector(_foundation1Position);
    drawEmptyVector(_foundation2Position);
    drawEmptyVector(_foundation3Position);
    drawEmptyVector(_foundation4Position);
    drawEmptyVector(_tableau1Position);
    drawEmptyVector(_tableau2Position);
    drawEmptyVector(_tableau3Position);
    drawEmptyVector(_tableau4Position);
    drawEmptyVector(_tableau5Position);
    drawEmptyVector(_tableau6Position);
    drawEmptyVector(_tableau7Position);
}

void GameViewModel::removeItems()
{
    for (auto viewModel : _cards)
    {
        _scene->removeItem(viewModel);
        delete viewModel;
    }
    _cards.clear();

    for (auto button : _buttons)
    {
        QGraphicsProxyWidget* proxy = nullptr;

        proxy = proxy->createProxyForChildWidget(button);
        if (proxy != nullptr)
        {
            _scene->removeItem(proxy);
            delete button;
        }
    }
    _buttons.clear();

    for (auto line : _lines)
    {
        _scene->removeItem(line);
        delete line;
    }
    _lines.clear();

    QGraphicsProxyWidget* proxy = nullptr;

    proxy = proxy->createProxyForChildWidget(_lblScore);
    if (proxy != nullptr)
    {
        _scene->removeItem(proxy);
        delete _lblScore;
    }

    proxy = proxy->createProxyForChildWidget(_lblMovesCounter);
    if (proxy != nullptr)
    {
        _scene->removeItem(proxy);
        delete _lblMovesCounter;
    }

    proxy = proxy->createProxyForChildWidget(_lblTotalGames);
    if (proxy != nullptr)
    {
        _scene->removeItem(proxy);
        delete _lblTotalGames;
    }

    proxy = proxy->createProxyForChildWidget(_lblTotalGamesWon);
    if (proxy != nullptr)
    {
        _scene->removeItem(proxy);
        delete _lblTotalGamesWon;
    }
}

void GameViewModel::drawBoard()
{
    for (auto cardViewModel : _cards)
    {
        cardViewModel->hide();
    }

    auto score = QString::number(_game->getScore());
    auto movesCounter = QString::number(_game->getMovesCounter());
    auto totalGames = QString::number(_game->getTotalGames());
    auto totalGamesWon = QString::number(_game->getTotalGamesWon());

    _lblScore->setText("Body: " + score);
    _lblMovesCounter->setText("Tahů: " + movesCounter);
    _lblTotalGames->setText("Hráno: " + totalGames);
    _lblTotalGamesWon->setText("Vyhráno: " + totalGamesWon);

    auto waste = _game->getCardVector(Waste);
    drawCardVector(waste, _wastePosition, 0);

    auto t1 = _game->getCardVector(Tableau1);
    drawCardVector(t1, _tableau1Position, 20);

    auto t2 = _game->getCardVector(Tableau2);
    drawCardVector(t2, _tableau2Position, 20);

    auto t3 = _game->getCardVector(Tableau3);
    drawCardVector(t3, _tableau3Position, 20);

    auto t4 = _game->getCardVector(Tableau4);
    drawCardVector(t4, _tableau4Position, 20);

    auto t5 = _game->getCardVector(Tableau5);
    drawCardVector(t5, _tableau5Position, 20);

    auto t6 = _game->getCardVector(Tableau6);
    drawCardVector(t6, _tableau6Position, 20);

    auto t7 = _game->getCardVector(Tableau7);
    drawCardVector(t7, _tableau7Position, 20);

    auto f1 = _game->getCardVector(Foundation1);
    drawCardVector(f1, _foundation1Position, 0);

    auto f2 = _game->getCardVector(Foundation2);
    drawCardVector(f2, _foundation2Position, 0);

    auto f3 = _game->getCardVector(Foundation3);
    drawCardVector(f3, _foundation3Position, 0);

    auto f4 = _game->getCardVector(Foundation4);
    drawCardVector(f4, _foundation4Position, 0);
}

void GameViewModel::drawCardVector(const vector<Card> &cards,
                                   const QPoint &position,
                                   unsigned int delta,
                                   unsigned int offset)
{
    for (unsigned int i = 0; i < cards.size(); ++i)
    {
        drawCard(cards.at(i),
                 true,
                 i + offset,
                 QPoint(position.x(), position.y() + i * delta));
    }
}

void GameViewModel::drawCard(const Card &card,
                             bool isVisible,
                             int zIndex,
                             const QPoint &position)
{
        unsigned int index = findCard(card);
        auto model = _cards.at(index);

        model->setOrientation(card.getCardOrientation());
        model->setPosition(position.x(), position.y());
        model->setZValue(zIndex);

        if (isVisible)
        {
            model->show();
        }
        else
        {
            model->hide();
        }
}

void GameViewModel::drawEmptyVector(const QPoint &position)
{
    QPen pen(QColor(Qt::green));

    auto line1 = _scene->addLine(position.x(),
                                 position.y(),
                                 position.x() + 87,
                                 position.y(),
                                 pen);
    auto line2 = _scene->addLine(position.x(),
                                 position.y(),
                                 position.x(),
                                 position.y() + 128,
                                 pen);
    auto line3 = _scene->addLine(position.x() + 87,
                                 position.y(),
                                 position.x() + 87,
                                 position.y() + 128,
                                 pen);
    auto line4 = _scene->addLine(position.x(),
                                 position.y() + 128,
                                 position.x() + 87,
                                 position.y() + 128,
                                 pen);

    line1->setZValue(-1);
    line2->setZValue(-1);
    line3->setZValue(-1);
    line4->setZValue(-1);

    _lines.push_back(line1);
    _lines.push_back(line2);
    _lines.push_back(line3);
    _lines.push_back(line4);
}

void GameViewModel::drawHelp(const QPoint &position)
{
    QPen pen(QBrush(Qt::GlobalColor::red), 5);

    auto line = _scene->addLine(position.x(),
                                position.y(),
                                position.x() + 88,
                                position.y(),
                                pen);
    line->setZValue(10000000);
    _helpLines.push_back(line);
}

void GameViewModel::clearHelp()
{
    for (auto helpLine : _helpLines)
    {
        _scene->removeItem(helpLine);
        delete helpLine;
    }
    _helpLines.clear();
}

CardVectorNames GameViewModel::getVectorName(int x, int y)
{
    const int cardWidth = 88;
    const int cardHeight = 128;
    const int colOffset = 500;

    if (x >= _wastePosition.x() &&
        x <= _wastePosition.x() + cardWidth &&
        y >= _wastePosition.y() &&
        y <= _wastePosition.y() + cardHeight)
    {
        return Waste;
    }
    else if (x >= _foundation1Position.x() &&
             x <= _foundation1Position.x() + cardWidth &&
             y >= _foundation1Position.y() &&
             y <= _foundation1Position.y() + cardHeight)
    {
        return Foundation1;
    }
    else if (x >= _foundation2Position.x() &&
             x <= _foundation2Position.x() + cardWidth &&
             y >= _foundation2Position.y() &&
             y <= _foundation2Position.y() + cardHeight)
    {
        return Foundation2;
    }
    else if (x >= _foundation3Position.x() &&
             x <= _foundation3Position.x() + cardWidth &&
             y >= _foundation3Position.y() &&
             y <= _foundation3Position.y() + cardHeight)
    {
        return Foundation3;
    }
    else if (x >= _foundation4Position.x() &&
             x <= _foundation4Position.x() + cardWidth &&
             y >= _foundation4Position.y() &&
             y <= _foundation4Position.y() + cardHeight)
    {
        return Foundation4;
    }
    else if (x >= _tableau1Position.x() &&
             x <= _tableau1Position.x() + cardWidth &&
             y >= _tableau1Position.y() &&
             y <= _tableau1Position.y() + colOffset)
    {
        return Tableau1;
    }
    else if (x >= _tableau2Position.x() &&
             x <= _tableau2Position.x() + cardWidth &&
             y >= _tableau2Position.y() &&
             y <= _tableau2Position.y() + colOffset)
    {
        return Tableau2;
    }
    else if (x >= _tableau3Position.x() &&
             x <= _tableau3Position.x() + cardWidth &&
             y >= _tableau3Position.y() &&
             y <= _tableau3Position.y() + colOffset)
    {
        return Tableau3;
    }
    else if (x >= _tableau4Position.x() &&
             x <= _tableau4Position.x() + cardWidth &&
             y >= _tableau4Position.y() &&
             y <= _tableau4Position.y() + colOffset)
    {
        return Tableau4;
    }
    else if (x >= _tableau5Position.x() &&
             x <= _tableau5Position.x() + cardWidth &&
             y >= _tableau5Position.y() &&
             y <= _tableau5Position.y() + colOffset)
    {
        return Tableau5;
    }
    else if (x >= _tableau6Position.x() &&
             x <= _tableau6Position.x() + cardWidth &&
             y >= _tableau6Position.y() &&
             y <= _tableau6Position.y() + colOffset)
    {
        return Tableau6;
    }
    else if (x >= _tableau7Position.x() &&
             x <= _tableau7Position.x() + cardWidth &&
             y >= _tableau7Position.y() &&
             y <= _tableau7Position.y() + colOffset)
    {
        return Tableau7;
    }
    else
    {
        return Undefined;
    }
}

unsigned int GameViewModel::findCard(const Card &findCard)
{
    unsigned int srcIndex = find_if(_cards.begin(), _cards.end(),
                                    [=](CardViewModel* c)
    {
        return c->getCoreCard().getCardColor() == findCard.getCardColor() &&
               c->getCoreCard().getCardValue() == findCard.getCardValue();
    }) - _cards.begin();

    return srcIndex;
}

void GameViewModel::printGameIsCompleted()
{
    if (_game->isGameCompleted())
    {
        QMessageBox msgBox;
        msgBox.setText("Hra je vyhrána! Celkové skore " +
                       QString::number(_game->getScore()) + ".");
        msgBox.exec();
    }
}
