/**
* @file   GameViewModel.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __GAMEVIEWMODEL_H_
#define __GAMEVIEWMODEL_H_

#include <QtCore>
#include <QtWidgets>
#include <vector>
#include "Game.h"
#include "CardViewModel.h"
#include <iostream>
#include "CardVectorNames.h"

using namespace std;
using namespace core;

/**
 * @brief The GameViewModel class
 */
class GameViewModel : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief GameViewModel constructor.
     * @param Scene pointer.
     * @param Game pointer.
     * @param Game ID.
     * @param Gameboard X offset.
     * @param Gameboard Y offset.
     */
    GameViewModel(QGraphicsScene* scene,
                  Game* game,
                  unsigned int gameNumber,
                  unsigned int xOffset = 0,
                  unsigned int yOffset = 0);

    /**
     * @brief GameViewModel destructor.
     */
    ~GameViewModel();

    /**
     * @brief Set Gameboard offset.
     * @param X.
     * @param Y.
     */
    void setGameBoardOffset(unsigned int x, unsigned int y);

public slots:
    /**
     * @brief Card selected slot.
     * @param Card view model.
     * @param X.
     * @param Y.
     */
    void cardSelectedSlot(CardViewModel* card, unsigned int x, unsigned int y);

    /**
     * @brief Card moved slot.
     * @param X.
     * @param Y.
     */
    void cardMovedSlot(CardViewModel*, unsigned int x, unsigned int y);

    /**
     * @brief Card released slot.
     * @param Card view model.
     * @param X.
     * @param Y.
     */
    void cardReleasedSlot(CardViewModel* card, unsigned int x, unsigned int y);

    /**
     * @brief Card right clicked slot.
     * @param Card view model.
     * @param X.
     * @param Y.
     */
    void cardRClickedSlot(CardViewModel* card, unsigned int x, unsigned int y);

    /**
     * @brief Next card in waste slot.
     */
    void nextCardInWasteSlot();

    /**
     * @brief Open game slot.
     */
    void openGameSlot();

    /**
     * @brief Save game slot.
     */
    void saveGameSlot();

    /**
     * @brief Undo slot.
     */
    void undoSlot();

    /**
     * @brief New game slot.
     */
    void newGameSlot();

    /**
     * @brief Help slot.
     */
    void helpSlot();

private:
    void init();
    void addItems();
    void removeItems();
    void drawBoard();
    void drawCardVector(const vector<Card>& cards,
                        const QPoint& position,
                        unsigned int delta,
                        unsigned int offset = 0);
    void drawCard(const Card& card,
                  bool isVisible,
                  int zIndex,
                  const QPoint& position);
    void drawEmptyVector(const QPoint& position);
    void drawHelp(const QPoint& position);
    void clearHelp();
    CardVectorNames getVectorName(int x, int y);
    unsigned int findCard(const Card& findCard);
    void printGameIsCompleted();

    QGraphicsScene* _scene;
    Game* _game;
    unsigned int _xOffset;
    unsigned int _yOffset;
    vector<CardViewModel*> _cards;
    vector<QPushButton*> _buttons;
    QLabel* _lblScore;
    QLabel* _lblMovesCounter;
    QLabel* _lblTotalGames;
    QLabel* _lblTotalGamesWon;
    vector<QGraphicsLineItem*> _lines;
    vector<QGraphicsLineItem*> _helpLines;
    vector<Card> _movedCards;
    unsigned int _gameNumber;
    QPoint _wastePosition;
    QPoint _wasteButtonPosition;
    QPoint _tableau1Position;
    QPoint _tableau2Position;
    QPoint _tableau3Position;
    QPoint _tableau4Position;
    QPoint _tableau5Position;
    QPoint _tableau6Position;
    QPoint _tableau7Position;
    QPoint _foundation1Position;
    QPoint _foundation2Position;
    QPoint _foundation3Position;
    QPoint _foundation4Position;
    QPoint _scorePosition;
    QPoint _movesCounterPosition;
    QPoint _totalGamesPosition;
    QPoint _totalGamesWonPosition;
    CardVectorNames _sourceName;
    CardVectorNames _destinationName;
    const QString _lblStyleSheet =
            "QLabel {background-color: rgb(116, 188, 101);}";
};

#endif
