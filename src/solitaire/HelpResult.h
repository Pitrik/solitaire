/**
* @file   HelpResult.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __HELPRESULT_
#define __HELPRESULT_

#include "CardVectorNames.h"

namespace core
{
    /**
     * @brief The HelpResult class
     */
    class HelpResult
	{
	public:
        /**
         * @brief HelpResult constructor.
         */
        HelpResult();

        /**
         * @brief HelpResult constructor.
         * @param Source.
         * @param Destination.
         */
        HelpResult(CardVectorNames source, CardVectorNames destination);

        CardVectorNames source;
        CardVectorNames destination;
	};
}

#endif
