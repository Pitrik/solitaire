/**
* @file   Game.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include <ctime>
#include "Game.h"
#include "ErrorCodes.h"
#include "HelpResult.h"

namespace core
{
    Game::Game()
    {
        _totalGames = 1;
        _totalGamesWon = 0;

        initCards();
        initGame();
    }

    Game::~Game()
    {
        clearAll();
    }

    vector<Card*> Game::getCards(unsigned offset, unsigned count)
    {
        return vector<Card *>(_allCards.begin() + offset,
                              _allCards.begin() + offset + count);
    }

    vector<Card*> Game::getCards(unsigned offset)
    {
        return vector<Card *>(_allCards.begin() + offset, _allCards.end());
    }

    vector<Card> Game::getCards()
    {
        vector<Card> result;

        for (unsigned int i = 0; i < _allCards.size(); ++i)
        {
            auto card = _allCards.at(i);
            result.push_back(Card(card->getCardColor(),
                                  card->getCardValue(),
                                  card->getCardOrientation()));
        }

        return result;
    }

    void Game::initCards()
    {
        _allCards.clear();

        for (unsigned int cardColor = 0; cardColor < 4; cardColor++)
        {
            for (unsigned int cardValue = 0; cardValue < 13; cardValue++)
            {
                _allCards.push_back(new Card(static_cast<CardColor>(cardColor),
                                             static_cast<CardValue>(cardValue),
                                             Bottom));
            }
        }
    }

    void Game::initGame()
    {
        srand(static_cast<unsigned int>(time(nullptr)));
        random_shuffle(_allCards.begin(), _allCards.end());

        for(auto card : _allCards)
        {
            card->setCardOrientation(Bottom);
        }

        _score = 0;
        _movesCounter = 0;

        _tableau1.clear();
        _tableau2.clear();
        _tableau3.clear();
        _tableau4.clear();
        _tableau5.clear();
        _tableau6.clear();
        _tableau7.clear();
        _foundation1.clear();
        _foundation2.clear();
        _foundation3.clear();
        _foundation4.clear();
        _waste.clear();
        _usedWaste.clear();

        _tableau1 = getCards(0, 1);
        _tableau1.back()->changeCardOrientation();

        _tableau2 = getCards(1, 2);
        _tableau2.back()->changeCardOrientation();

        _tableau3 = getCards(3, 3);
        _tableau3.back()->changeCardOrientation();

        _tableau4 = getCards(6, 4);
        _tableau4.back()->changeCardOrientation();

        _tableau5 = getCards(10, 5);
        _tableau5.back()->changeCardOrientation();

        _tableau6 = getCards(15, 6);
        _tableau6.back()->changeCardOrientation();

        _tableau7 = getCards(21, 7);
        _tableau7.back()->changeCardOrientation();

        _waste = getCards(28);
        for (auto card : _waste)
        {
            card->changeCardOrientation();
        }
    }

    Card* Game::getCardFrom(CardVectorNames src)
    {
        if (!this->getCardVectorR(src)->empty())
        {
            return this->getCardVectorR(src)->back();
        }

        return nullptr;
    }

    Card* Game::getCardFrom(unsigned int cardIndex, CardVectorNames src)
    {
        auto cards = this->getCardVectorR(src);

        if (cards->size() > cardIndex)
        {
            return cards->at(cardIndex);
        }

        return nullptr;
    }

    vector<Card*> Game::getCardsFrom(unsigned int offset, CardVectorNames src)
    {
        auto cardVector = getCardVectorR(src);
        return vector<Card *>(cardVector->begin() + offset, cardVector->end());
    }

    vector<Card *> *Game::getCardVectorR(CardVectorNames name)
    {
        switch (name)
        {
        case Waste:
            return &_usedWaste;
        case Foundation1:
            return &_foundation1;
        case Foundation2:
            return &_foundation2;
        case Foundation3:
            return &_foundation3;
        case Foundation4:
            return &_foundation4;
        case Tableau1:
            return &_tableau1;
        case Tableau2:
            return &_tableau2;
        case Tableau3:
            return &_tableau3;
        case Tableau4:
            return &_tableau4;
        case Tableau5:
            return &_tableau5;
        case Tableau6:
            return &_tableau6;
        case Tableau7:
            return &_tableau7;
        default:
            throw NotFound;
        }
    }

    void Game::nextCardInWaste()
    {
        _prevGameStates.push_back(getState());
        _movesCounter++;

        if (!_waste.empty())
        {
            auto card = _waste.back();
            _usedWaste.push_back(card);
            _waste.pop_back();
        }
        else    //recycle waste
        {
            _waste = _usedWaste;
            _usedWaste.clear();
            reverse(_waste.begin(), _waste.end());
            addScore(-100);
        }
    }

    bool Game::isGameCompleted() const
    {
        if (_foundation1.size() == 13 &&
            _foundation2.size() == 13 &&
            _foundation3.size() == 13 &&
            _foundation4.size() == 13)
        {
            return true;
        }

        return false;
    }

    HelpResult Game::help()
    {
        //check if exists move from tableau to foundation
        for (unsigned int i = Tableau1; i <= Tableau7; ++i)
        {
            auto srcCard = getCardFrom(static_cast<CardVectorNames>(i));
            if (srcCard == nullptr)
            {
                continue;
            }

            for (unsigned int j = Foundation1; j <= Foundation4; ++j)
            {
                if (cardMoveLogic(Card(srcCard->getCardColor(),
                                       srcCard->getCardValue(),
                                       srcCard->getCardOrientation()),
                                  static_cast<CardVectorNames>(i),
                                  static_cast<CardVectorNames>(j)))
                {
                    return HelpResult(static_cast<CardVectorNames>(i),
                                      static_cast<CardVectorNames>(j));
                }
            }
        }

        auto srcCard = getCardFrom(Waste);
        if (srcCard != nullptr)
        {
            //check if exists move from waste to foundation
            for (unsigned int i = Foundation1; i <= Foundation4; ++i)
            {
                if (cardMoveLogic(Card(srcCard->getCardColor(),
                                       srcCard->getCardValue(),
                                       srcCard->getCardOrientation()),
                                  Waste,
                                  static_cast<CardVectorNames>(i)))
                {
                    return HelpResult(Waste, static_cast<CardVectorNames>(i));
                }
            }

            //check if exists move from waste to tableau
            for (unsigned int i = Tableau1; i <= Tableau7; ++i)
            {
                if (cardMoveLogic(Card(srcCard->getCardColor(),
                                       srcCard->getCardValue(),
                                       srcCard->getCardOrientation()),
                                  Waste,
                                  static_cast<CardVectorNames>(i)))
                {
                    return HelpResult(Waste, static_cast<CardVectorNames>(i));
                }
            }
        }

        //check if exists move between tableau
        for (unsigned int i = Tableau1; i <= Tableau7; ++i)
        {
            srcCard = getCardFrom(static_cast<CardVectorNames>(i));
            if (srcCard == nullptr)
            {
                continue;
            }

            for (unsigned int j = Tableau1; j <= Tableau7; ++j)
            {
                if (cardMoveLogic(Card(srcCard->getCardColor(),
                                       srcCard->getCardValue(),
                                       srcCard->getCardOrientation()),
                                  static_cast<CardVectorNames>(i),
                                  static_cast<CardVectorNames>(j)))
                {
                    return HelpResult(static_cast<CardVectorNames>(i),
                                      static_cast<CardVectorNames>(j));
                }
            }
        }

        return HelpResult(Undefined, Undefined);
    }

    void Game::undo()
    {
        if (_prevGameStates.empty())
        {
            return;
        }

        clearAll();
        setState(_prevGameStates.back());
        addScore(-2);
        _prevGameStates.pop_back();
    }

    void Game::saveGame(const string &filename) const
    {
        if (filename.size() == 0)
        {
            return;
        }

        ofstream os;

        os.open(filename, ios_base::out);
        if (os.is_open())
        {
            os << _score << endl;
            os << _movesCounter << endl;
            os << _totalGames << endl;
            os << _totalGamesWon << endl;
            os << serializeVector(_waste);
            os << serializeVector(_usedWaste);
            os << serializeVector(_tableau1);
            os << serializeVector(_tableau2);
            os << serializeVector(_tableau3);
            os << serializeVector(_tableau4);
            os << serializeVector(_tableau5);
            os << serializeVector(_tableau6);
            os << serializeVector(_tableau7);
            os << serializeVector(_foundation1);
            os << serializeVector(_foundation2);
            os << serializeVector(_foundation3);
            os << serializeVector(_foundation4);

            os.close();
        }
        else
        {
            throw FileError;
        }
    }

    void Game::openGame(const string &filename)
    {
        ifstream is;

        is.open(filename);
        if (is.is_open())
        {
            clearAll();

            string line;

            is >> _score;
            is >> _movesCounter;
            is >> _totalGames;
            is >> _totalGamesWon;

            is >> line;
            _waste = deserializeVector(line);
            is >> line;
            _usedWaste = deserializeVector(line);
            is >> line;
            _tableau1 = deserializeVector(line);
            is >> line;
            _tableau2 = deserializeVector(line);
            is >> line;
            _tableau3 = deserializeVector(line);
            is >> line;
            _tableau4 = deserializeVector(line);
            is >> line;
            _tableau5 = deserializeVector(line);
            is >> line;
            _tableau6 = deserializeVector(line);
            is >> line;
            _tableau7 = deserializeVector(line);
            is >> line;
            _foundation1 = deserializeVector(line);
            is >> line;
            _foundation2 = deserializeVector(line);
            is >> line;
            _foundation3 = deserializeVector(line);
            is >> line;
            _foundation4 = deserializeVector(line);

            is.close();
        }
        else
        {
            throw FileError;
        }
    }

    void Game::newGame()
    {
        _totalGames++;

        clearAll();
        initCards();
        initGame();
    }

    int Game::getScore() const
    {
        return _score;
    }

    int Game::getTotalGamesWon() const
    {
        return _totalGamesWon;
    }

    int Game::getTotalGames() const
    {
        return _totalGames;
    }

    int Game::getMovesCounter() const
    {
        return _movesCounter;
    }

    vector<Card> Game::getCardVector(CardVectorNames name)
    {
        vector<Card> result;

        auto cards = getCardVectorR(name);
        for (unsigned int i = 0; i < cards->size(); ++i)
        {
            auto card = cards->at(i);
            result.push_back(Card(card->getCardColor(),
                                  card->getCardValue(),
                                  card->getCardOrientation()));
        }

        return result;
    }

    vector<Card> Game::getCardsForMoving(const Card &sourceCard,
                                         CardVectorNames src)
    {
        vector<Card> result;

        auto sourceCardVector = this->getCardVectorR(src);
        auto srcIndex = this->findCard(sourceCardVector, sourceCard);
        auto cardsForMoving = getCardsFrom(srcIndex, src);

        for(unsigned int i = 0; i < cardsForMoving.size(); ++i)
        {
            auto card = cardsForMoving.at(i);
            result.push_back(Card(card->getCardColor(),
                                  card->getCardValue(),
                                  card->getCardOrientation()));
        }

        return result;
    }

    unsigned int Game::findCard(vector<Card*>* cards, const Card &findCard)
    {
        unsigned int srcIndex = find_if(cards->begin(), cards->end(),
                                        [=](Card* c)
        {
            return c->getCardColor() == findCard.getCardColor() &&
                   c->getCardValue() == findCard.getCardValue();
        }) - cards->begin();

        if (srcIndex >= cards->size())
        {
            throw NotFound;
        }

        return srcIndex;
    }

    void Game::cardMoveExec(const Card &sourceCard,
                            CardVectorNames src,
                            CardVectorNames dst)
    {
        if (!cardMoveLogic(sourceCard, src, dst))
        {
            return;     //move is not valid
        }

        auto sourceCardVector = this->getCardVectorR(src);
        auto destinationCardVector = this->getCardVectorR(dst);
        auto srcIndex = this->findCard(sourceCardVector, sourceCard);

        _prevGameStates.push_back(getState());
        _movesCounter++;

        //Score compution.
        if (dst == Foundation1 ||
            dst == Foundation2 ||
            dst == Foundation3 ||
            dst == Foundation4)
        {
            addScore(10);
        }

        if (src == Waste)
        {
            addScore(5);
        }
        else if (src == Foundation1 ||
                 src == Foundation2 ||
                 src == Foundation3 ||
                 src == Foundation4)
        {
            addScore(-15);
        }

        //Cards moving.
        for (auto i = srcIndex; i < sourceCardVector->size(); ++i)
        {
            auto card = sourceCardVector->at(i);
            destinationCardVector->push_back(card);
        }

        for (auto i = srcIndex; i < sourceCardVector->size(); )
        {
            this->getCardVectorR(src)->pop_back();
        }

        if (!getCardVectorR(src)->empty())
        {
            auto lastCard = getCardVectorR(src)
                    ->at(getCardVectorR(src)->size() - 1);
            if (lastCard->getCardOrientation() != Top)	//Turn last card in col.
            {
                lastCard->changeCardOrientation();
                addScore(5);
            }
        }

        if (isGameCompleted())
        {
            _totalGamesWon++;
        }
    }

    bool Game::cardMoveLogic(const Card &sourceCard,
                             CardVectorNames src,
                             CardVectorNames dst)
    {
        auto sourceCardVector = this->getCardVectorR(src);
        auto srcIndex = this->findCard(sourceCardVector, sourceCard);
        auto srcCard = this->getCardFrom(srcIndex, src);
        auto dstCard = this->getCardFrom(dst);

        if (dst == Waste)       //You can not move card to waste.
        {
            return false;
        }

        if (src == Waste)       //You can get last card in waste.
        {
            if (srcCard != _usedWaste.back())
            {
                return false;
            }
        }

        if (dstCard != nullptr) //Destination col contains some cards.
        {
            if (dst == Foundation1 ||
                dst == Foundation2 ||
                dst == Foundation3 ||
                dst == Foundation4)   //Destination is foundation.
            {
                if (srcCard->getCardValue() - dstCard->getCardValue() != 1)
                {
                    return false;
                }
            }
            else if (dstCard->getCardValue() - srcCard->getCardValue() != 1)
            {
                return false;
            }

            if (srcCard->getCardOrientation() != Top ||
                dstCard->getCardOrientation() != Top) //Booth cards must be top.
            {
                return false;
            }

            if (dst == Foundation4 ||
                dst == Foundation3 ||
                dst == Foundation1 ||
                dst == Foundation2)
            {
                //Cards must have some color.
                if (srcCard->getCardColor() != dstCard->getCardColor())
                {
                    return false;
                }

                //And card must be last in source col.
                if (srcCard != sourceCardVector->back())
                {
                    return false;
                }
            }
            else        //Cards must have different colors in tableau col.
            {
                if (srcCard->getCardColor() == Hearts ||
                    srcCard->getCardColor() == Diamonds)
                {
                    if (dstCard->getCardColor() != Spades &&
                        dstCard->getCardColor() != Clubs)
                    {
                        return false;
                    }
                }
                else
                {
                    if (dstCard->getCardColor() != Hearts &&
                        dstCard->getCardColor() != Diamonds)
                    {
                        return false;
                    }
                }
            }
        }
        else if (dst == Foundation1 ||
                 dst == Foundation2 ||
                 dst == Foundation3 ||
                 dst == Foundation4)		//First card.
        {
            if (srcCard->getCardValue() != VA)	//VA only for foundation col.
            {
                return false;
            }
        }
        else
        {
            if (srcCard->getCardValue() != VK)	//VK only for tableau col.
            {
                return false;
            }
        }

        return true;    //All is valid
    }

    void Game::addScore(int points)
    {
        _score += points;
        if (_score < 0)
        {
            _score = 0;
        }
    }

    vector<vector<Card>> Game::getState()
    {
        vector<vector<Card>> state;

        state.push_back(getVectorState(_waste));
        state.push_back(getVectorState(_usedWaste));
        state.push_back(getVectorState(_tableau1));
        state.push_back(getVectorState(_tableau2));
        state.push_back(getVectorState(_tableau3));
        state.push_back(getVectorState(_tableau4));
        state.push_back(getVectorState(_tableau5));
        state.push_back(getVectorState(_tableau6));
        state.push_back(getVectorState(_tableau7));
        state.push_back(getVectorState(_foundation1));
        state.push_back(getVectorState(_foundation2));
        state.push_back(getVectorState(_foundation3));
        state.push_back(getVectorState(_foundation4));

        return state;
    }

    void Game::setState(const vector<vector<Card> > &state)
    {
        _waste = getVectorState(state.at(0));
        _usedWaste = getVectorState(state.at(1));
        _tableau1 = getVectorState(state.at(2));
        _tableau2 = getVectorState(state.at(3));
        _tableau3 = getVectorState(state.at(4));
        _tableau4 = getVectorState(state.at(5));
        _tableau5 = getVectorState(state.at(6));
        _tableau6 = getVectorState(state.at(7));
        _tableau7 = getVectorState(state.at(8));
        _foundation1 = getVectorState(state.at(9));
        _foundation2 = getVectorState(state.at(10));
        _foundation3 = getVectorState(state.at(11));
        _foundation4 = getVectorState(state.at(12));
    }

    vector<Card> Game::getVectorState(const vector<Card *> &cards)
    {
        vector<Card> state;

        for (auto card : cards)
        {
            state.push_back(Card(card->getCardColor(),
                                 card->getCardValue(),
                                 card->getCardOrientation()));
        }

        return state;
    }

    vector<Card*> Game::getVectorState(const vector<Card> &state)
    {
        vector<Card*> context;

        for (auto card : state)
        {
            context.push_back(new Card(card.getCardColor(),
                                       card.getCardValue(),
                                       card.getCardOrientation()));
        }

        return context;
    }

    string Game::serializeVector(const vector<Card *> &cards)
    {
        stringstream ss;

        if (cards.size() == 0)
        {
            ss << "EMPTY";
        }

        for (unsigned int i = 0; i < cards.size(); ++i)
        {
            ss << serializeCard(cards.at(i));

            if (i < cards.size() - 1)
            {
                ss << ";";
            }
        }
        ss << endl;

        return ss.str();
    }

    vector<Card *> Game::deserializeVector(const string &line) const
    {
        vector<Card*> cardVector;

        if (line == "EMPTY")
        {
            return cardVector;
        }

        auto parsedLine = extensions::split(line, ";");
        for (auto parsedCard : parsedLine)
        {
            auto card = deserializeCard(parsedCard);
            cardVector.push_back(card);
        }

        return cardVector;
    }

    string Game::serializeCard(Card *card)
    {
        stringstream ss;

        ss << card->getCardColor();
        ss << "_";
        ss << card->getCardValue();
        ss << "_";
        ss << card->getCardOrientation();

        return ss.str();
    }

    Card* Game::deserializeCard(const string &line) const
    {
        auto parsedLine = extensions::split(line, "_");
        if (parsedLine.size() != 3)
        {
            throw FileError;
        }

        return new Card(static_cast<CardColor>(stoi(parsedLine[0])),
                        static_cast<CardValue>(stoi(parsedLine[1])),
                        static_cast<CardOrientation>(stoi(parsedLine[2])));
    }

    void Game::clearAll()
    {
        for (auto card : _waste)
        {
            delete card;
        }
        _waste.clear();

        for (auto card : _usedWaste)
        {
            delete card;
        }
        _usedWaste.clear();

        for (auto card : _tableau1)
        {
            delete card;
        }
        _tableau1.clear();

        for (auto card : _tableau2)
        {
            delete card;
        }
        _tableau2.clear();

        for (auto card : _tableau3)
        {
            delete card;
        }
        _tableau3.clear();

        for (auto card : _tableau4)
        {
            delete card;
        }
        _tableau4.clear();

        for (auto card : _tableau5)
        {
            delete card;
        }
        _tableau5.clear();

        for (auto card : _tableau6)
        {
            delete card;
        }
        _tableau6.clear();

        for (auto card : _tableau7)
        {
            delete card;
        }
        _tableau7.clear();

        for (auto card : _foundation1)
        {
            delete card;
        }
        _foundation1.clear();

        for (auto card : _foundation2)
        {
            delete card;
        }
        _foundation2.clear();

        for (auto card : _foundation3)
        {
            delete card;
        }
        _foundation3.clear();

        for (auto card : _foundation4)
        {
            delete card;
        }
        _foundation4.clear();
    }
}
