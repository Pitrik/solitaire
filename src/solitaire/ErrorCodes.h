/**
* @file   ErrorCodes.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __ERRORCODES_
#define __ERRORCODES_

namespace core
{
    /**
     * @brief Enumeration of error codes.
     */
	typedef enum
	{
		BadCommand,
		BadConvert,
		NotFound,
		FileError
	} ErrorCodes;
}

#endif
