/**
 * @file   Helpers.h
 * @author Martin Pitrik
 * @date   March, 2017
 */

#ifndef __HELPERS_
#define __HELPERS_

#include <string>
#include <vector>
#include <sstream>

#include "Card.h"
#include "Game.h"
#include "CardVectorNames.h"

using namespace std;
using namespace core;

namespace Helpers
{
    /**
     * @brief Card to string.
     * @param Card.
     * @return Card as string.
     */
    string cardToString(Card card);

    /**
     * @brief CardVector to string.
     * @param CardVector.
     * @param VectorName.
     * @return CardVector as string.
     */
    string cardVectorToString(vector<Card> cardVector, string name);

    /**
     * @brief String to card.
     * @param Card string representation.
     * @return Card.
     */
    Card stringToCard(string card);

    /**
     * @brief String to VectorName
     * @param String name.
     * @return Value from CardVectorNames.
     */
    CardVectorNames stringToVectorName(string name);

    /**
     * @brief VectorName to string.
     * @param CardVector name.
     * @return String name.
     */
    string vectorNameToString(CardVectorNames name);
}

#endif
