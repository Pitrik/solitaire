/**
* @file   SolitaireWindow.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __GAMEBOARD_H_
#define __GAMEBOARD_H_

#include <QtCore>
#include <QtWidgets>
#include "Game.h"
#include "GameViewModel.h"

/**
 * @brief The SolitaireWindow class
 */
class SolitaireWindow : public QGraphicsView
{
    Q_OBJECT
public:
    /**
     * @brief SolitaireWindow constructor.
     * @param Scene pointer.
     * @param Parent, default is 0.
     */
    SolitaireWindow(QGraphicsScene *scene, QWidget *parent = 0);

    /**
     * @brief SolitaireWindow destructor.
     */
    ~SolitaireWindow();

    /**
     * @brief Add game to scene.
     */
    void addGame();

    /**
     * @brief Delete game from scene.
     */
    void deleteGame();

public slots:
    /**
     * @brief New gameboard slot.
     */
    void newGameBoardSlot();

    /**
     * @brief Close gameboard slot.
     */
    void closeGameBoardSlot();

private:
    Game* _game1;
    Game* _game2;
    Game* _game3;
    Game* _game4;
    GameViewModel* _model1;
    GameViewModel* _model2;
    GameViewModel* _model3;
    GameViewModel* _model4;
    QGraphicsScene* _scene;
    QPushButton* _btnNewGameBoard;
    QPushButton* _btnCloseGameBoard;
};

#endif
