/**
* @file   main.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include <QtCore>
#include <QtWidgets>

#include "CardViewModel.h"
#include "SolitaireWindow.h"
#include "GameViewModel.h"
#include "Game.h"

using namespace std;
using namespace core;

int main(int argc, char **argv)
{
    Q_INIT_RESOURCE(images);

    QApplication app(argc, argv);
    QGraphicsScene scene;
    SolitaireWindow window(&scene);

    window.setFrameStyle(0);
    window.setAlignment(Qt::AlignLeft | Qt::AlignTop);
    window.setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    window.setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    window.setWindowTitle("Solitaire");
    window.setWindowIcon(QIcon(QPixmap(":/images/icon.png")));
    window.resize(700, 600);
    window.show();

    return app.exec();
}
