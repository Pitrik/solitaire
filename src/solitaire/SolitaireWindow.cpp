/**
* @file   SolitaireWindow.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "SolitaireWindow.h"

SolitaireWindow::SolitaireWindow(QGraphicsScene *scene,
                                 QWidget *parent)
    : QGraphicsView(scene, parent)
{
    _scene = scene;

    _game1 = nullptr;
    _game2 = nullptr;
    _game3 = nullptr;
    _game4 = nullptr;
    _model1 = nullptr;
    _model2 = nullptr;
    _model3 = nullptr;
    _model4 = nullptr;

    _btnNewGameBoard = new QPushButton("Nová hrací plocha");
    _btnCloseGameBoard = new QPushButton("Zavřít hrací plochu");

    _btnNewGameBoard->setGeometry((QRect(QPoint(0, 0), QSize(140, 50))));
    _btnCloseGameBoard->setGeometry((QRect(QPoint(140, 0), QSize(140, 50))));

    QObject::connect(_btnNewGameBoard,
                     SIGNAL(released()),
                     this,
                     SLOT(newGameBoardSlot()));
    QObject::connect(_btnCloseGameBoard,
                     SIGNAL(released()),
                     this,
                     SLOT(closeGameBoardSlot()));

    _scene->setBackgroundBrush(QColor(116, 188, 101));
    _scene->addWidget(_btnNewGameBoard);
    _scene->addWidget(_btnCloseGameBoard);

    addGame();
}

SolitaireWindow::~SolitaireWindow()
{
    delete _btnNewGameBoard;
    delete _btnCloseGameBoard;
}

void SolitaireWindow::addGame()
{
    if (_game1 == nullptr)
    {
        _game1 = new Game();
        _model1 = new GameViewModel(_scene, _game1, 1, 0, 50);
    }
    else if (_game2 == nullptr)
    {
        _game2 = new Game();
        _model2 = new GameViewModel(_scene, _game2, 2, 800, 50);
    }
    else if (_game3 == nullptr)
    {
        _game3 = new Game();
        _model3 = new GameViewModel(_scene, _game3, 3, 0, 800);
    }
    else if (_game4 == nullptr)
    {
        _game4 = new Game();
        _model4 = new GameViewModel(_scene, _game4, 4, 800, 800);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Maximalně lze rozehrát čtyři hry.");
        msgBox.exec();
    }
}

void SolitaireWindow::deleteGame()
{
    if (_model4 != nullptr)
    {
        delete _model4;
        delete _game4;
        _model4 = nullptr;
        _game4 = nullptr;
    }
    else if (_model3 != nullptr)
    {
        delete _model3;
        delete _game3;
        _model3 = nullptr;
        _game3 = nullptr;
    }
    else if (_model2 != nullptr)
    {
        delete _model2;
        delete _game2;
        _model2 = nullptr;
        _game2 = nullptr;
    }
    else if (_model1 != nullptr)
    {
        delete _model1;
        delete _game1;
        _model1 = nullptr;
        _game1 = nullptr;
    }
}

void SolitaireWindow::closeGameBoardSlot()
{
    deleteGame();
}

void SolitaireWindow::newGameBoardSlot()
{
    addGame();
}
