QT += widgets

SOURCES = main.cpp \
    Card.cpp \
    Extensions.cpp \
    Game.cpp \
    HelpResult.cpp \
    CardViewModel.cpp \
    GameViewModel.cpp \
    SolitaireWindow.cpp

RESOURCES = \
    images.qrc

HEADERS += \
    CardViewModel.h \
    SolitaireWindow.h \
    GameViewModel.h \
    Card.h \
    CardVectorNames.h \
    ErrorCodes.h \
    Extensions.h \
    Game.h \
    HelpResult.h
	
CONFIG += c++11
