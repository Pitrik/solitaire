/**
* @file   Solitaire.cpp
* @author Martin Pitrik
* @date   March, 2017
* @brief  Solitaire Game CLI version.
*/

#include <iostream>

#include "Game.h"
#include "Helpers.h"
#include "Extensions.h"
#include "ErrorCodes.h"

using namespace std;
using namespace core;

int main()
{
    Game game;

    while (true)
    {
        if (game.isGameCompleted())
        {
            cout << "Vyhral jste! ";
            cout << "Vase skore je " << game.getScore() << ". Gratulujeme!";
            cout << endl;
            cout << "=======================================================";
            cout << endl;
            cout << "Volba: [N] - nova hra" << endl;
            cout << "Volba: [O] - otevri hru" << endl;
            cout << "Volba: [Q] - konec hry" << endl;
        }
        else
        {
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Waste),
                        "P") << endl << endl;;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau1),
                        "P1") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau2),
                        "P2") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau3),
                        "P3") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau4),
                        "P4") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau5),
                        "P5") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau6),
                        "P6") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Tableau7),
                        "P7") << endl;
            cout << "=======================================================";
            cout << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Foundation1),
                        "F1") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Foundation2),
                        "F2") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Foundation3),
                        "F3") << endl;
            cout << Helpers::cardVectorToString(
                        game.getCardVector(Foundation4),
                        "F4") << endl;
            cout << "=======================================================";
            cout << endl;
            cout << "Vase skore: " << game.getScore() << endl;
            cout << "=======================================================";
            cout << endl;
            cout << "Volba: [zdroj_karta_cil] - presun karty" << endl;
            cout << "Volba: [T] - dalsi karta v balicku" << endl;
            cout << "Volba: [B] - zpet" << endl;
            cout << "Volba: [H] - napoveda" << endl;
            cout << "Volba: [N] - nova hra" << endl;
            cout << "Volba: [S] - uloz hru" << endl;
            cout << "Volba: [O] - otevri hru" << endl;
            cout << "Volba: [Q] - konec hry" << endl;
        }

        cout << "Vybrana volba: ";

        string input;
        cin.clear();
        cin >> input;

        for (auto &c : input) c = toupper(c);

        if (input == "T")
        {
            game.nextCardInWaste();
        }
        else if (input == "Q")
        {
            break;
        }
        else if (input == "B")
        {
            game.undo();
        }
        else if (input == "N")
        {
            game.newGame();
        }
        else if (input == "S")
        {
            string filename;

            cout << "Nazev souboru: ";
            cin >> filename;

            game.saveGame(filename);
        }
        else if (input == "O")
        {
            string filename;

            cout << "Nazev souboru: ";
            cin >> filename;

            game.openGame(filename);
        }
        else if (input == "H")
        {
            auto help = game.help();

            cout << Helpers::vectorNameToString(help.source);
            cout << "->";
            cout << Helpers::vectorNameToString(help.destination) << endl;
        }
        else //tah, parsovani
        {
            auto move = extensions::split(input, "_");
            if (move.size() == 3)
            {
                try
                {
                    auto sourceName = Helpers::stringToVectorName(move.at(0));
                    auto destinationName = Helpers::stringToVectorName(move.at(2));
                    auto selectedCard = Helpers::stringToCard(move.at(1));

                    game.cardMoveExec(selectedCard, sourceName, destinationName);
                }
                catch (ErrorCodes code)
                {
                    cout << "Chyba: " << code << endl;
                }
            }
            else
            {
                cout << "Neplatny prikaz!" << endl;
            }
        }

        cout << endl << endl;
        cout << "=======================================================";
        cout << endl << endl;
    }

    return 0;
}
