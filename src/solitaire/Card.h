/**
 * @file   Card.h
 * @author Martin Pitrik
 * @date   March, 2017
 */

#ifndef __CARD__
#define __CARD__

#include <string>

using namespace std;

namespace core
{
    /**
     * @brief Enumeration for orientation of card.
     */
    typedef enum
    {
        Top,
        Bottom
    } CardOrientation;

    /**
     * @brief Enumeration for color of card.
     */
    typedef enum
    {
        Hearts,
        Spades,
        Clubs,
        Diamonds
    } CardColor;

    /**
     * @brief Enumeration for value of card.
     */
    typedef enum
    {
        VA,
        V2,
        V3,
        V4,
        V5,
        V6,
        V7,
        V8,
        V9,
        V10,
        VJ,
        VQ,
        VK
    } CardValue;

    /**
     * @brief The Card class
     */
    class Card
    {
    public:
        /**
         * @brief Card parametereless contrustor
         */
        Card();

        /**
         * @brief Card constructor.
         * @param Color of card.
         * @param Value of card.
         * @param Orientation of card.
         */
        Card(CardColor, CardValue, CardOrientation);

        /**
         * @brief Copy constructor
         * @param Card.
         */
        Card(const Card& card);

        /**
         * @brief Get color of card.
         * @retval Color.
         */
        CardColor getCardColor() const;

        /**
         * @brief Get value of card.
         * @retval Value.
         */
        CardValue getCardValue() const;

        /**
         * @brief Set card orientation.
         * @param Orientation.
         */
        void setCardOrientation(CardOrientation orientation);

        /**
         * @brief Get orientation of card.
         * @retval Orientation.
         */
        CardOrientation getCardOrientation() const;

        /**
         * @brief Change card orientation.
         */
        void changeCardOrientation();

    private:
        CardColor _color;
        CardValue _value;
        CardOrientation _orientation;
    };
}

#endif
