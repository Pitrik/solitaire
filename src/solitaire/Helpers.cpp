/**
 * @file   Helpers.cpp
 * @author Martin Pitrik
 * @date   March, 2017
 */

#include "Helpers.h"
#include "ErrorCodes.h"

namespace Helpers
{
    string cardToString(Card card)
	{
		string color;
		string value;

        if (card.getCardOrientation() != Top)
		{
			return "*";
		}

        switch (card.getCardColor())
		{
		case Clubs: color = "C"; break;
		case Diamonds: color = "D"; break;
		case Hearts: color = "H"; break;
		case Spades: color = "S"; break;
		}

        switch (card.getCardValue())
		{
		case VA: value = "A"; break;
		case V2: value = "2"; break;
		case V3: value = "3"; break;
		case V4: value = "4"; break;
		case V5: value = "5"; break;
		case V6: value = "6"; break;
		case V7: value = "7"; break;
		case V8: value = "8"; break;
		case V9: value = "9"; break;
		case V10: value = "10"; break;
		case VJ: value = "J"; break;
		case VQ: value = "Q"; break;
		case VK: value = "K"; break;
		}

		return color + value;
	}

    string cardVectorToString(vector<Card> cardVector, string name)
	{
		stringstream result;

		result << name + "\n";
        for (unsigned int i = 0; i < cardVector.size(); ++i)
		{
            result << "[" << i << "]" << ":" << cardToString(cardVector.at(i)) << "\t";
		}
		result << "\n";

		return result.str();
	}

	Card stringToCard(string card)
	{
		CardColor color;
		CardValue value;

		auto sColor = card[0];
		if (sColor == 'C')
		{
			color = Clubs;
		}
		else if (sColor == 'D')
		{
			color = Diamonds;
		}
		else if (sColor == 'H')
		{
			color = Hearts;
		}
		else if (sColor == 'S')
		{
			color = Spades;
		}
		else
		{
			throw BadConvert;
		}

		auto sValue = card.substr(1, card.size() - 1);

		if (sValue == "A")
		{
			value = VA;
		}
		else if (sValue == "2")
		{
			value = V2;
		}
		else if (sValue == "3")
		{
			value = V3;
		}
		else if (sValue == "4")
		{
			value = V4;
		}
		else if (sValue == "5")
		{
			value = V5;
		}
		else if (sValue == "6")
		{
			value = V6;
		}
		else if (sValue == "7")
		{
			value = V7;
		}
		else if (sValue == "8")
		{
			value = V8;
		}
		else if (sValue == "9")
		{
			value = V9;
		}
		else if (sValue == "10")
		{
			value = V10;
		}
		else if (sValue == "J")
		{
			value = VJ;
		}
		else if (sValue == "Q")
		{
			value = VQ;
		}
		else if (sValue == "K")
		{
			value = VK;
		}
		else
		{
			throw BadConvert;
		}

		return Card(color, value, Bottom);
	}

	CardVectorNames stringToVectorName(string name)
	{
		if (name == "P")
		{
			return Waste;
		}
		if (name == "P1")
		{
			return Tableau1;
		}
		if (name == "P2")
		{
			return Tableau2;
		}
		if (name == "P3")
		{
			return Tableau3;
		}
		if (name == "P4")
		{
			return Tableau4;
		}
		if (name == "P5")
		{
			return Tableau5;
		}
		if (name == "P6")
		{
			return Tableau6;
		}
		if (name == "P7")
		{
			return Tableau7;
		}
		if (name == "F1")
		{
			return Foundation1;
		}
		if (name == "F2")
		{
			return Foundation2;
		}
		if (name == "F3")
		{
			return Foundation3;
		}
		if (name == "F4")
		{
			return Foundation4;
		}

		throw BadConvert;
	}

    string vectorNameToString(CardVectorNames name)
    {
        switch(name)
        {
        case Waste:
            return "P";
        case Tableau1:
            return "P1";
        case Tableau2:
            return "P2";
        case Tableau3:
            return "P3";
        case Tableau4:
            return "P4";
        case Tableau5:
            return "P5";
        case Tableau6:
            return "P6";
        case Tableau7:
            return "P7";
        case Foundation1:
            return "F1";
        case Foundation2:
            return "F2";
        case Foundation3:
            return "F3";
        case Foundation4:
            return "F4";
        case Undefined:
            return "";
        }
    }
}
