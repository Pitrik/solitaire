/**
* @file   HelpResult.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "HelpResult.h"

namespace core
{ 
    HelpResult::HelpResult()
    {
        source = Undefined;
        destination = Undefined;
    }

    HelpResult::HelpResult(CardVectorNames source, CardVectorNames destination)
	{
        this->source = source;
        this->destination = destination;
	}
}
