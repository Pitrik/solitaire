/**
* @file   Extensions.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "Extensions.h"

namespace extensions
{
    vector<string> split(string text, const string &delimeter)
	{
		vector<string> output;
		auto position = string::npos;

		do
		{
			position = text.find(delimeter);
			output.push_back(text.substr(0, position));
			if (string::npos != position)
			{
				text = text.substr(position + delimeter.size());
			}
		} while (string::npos != position);

		return output;
	}
}
