/**
* @file   CardViewModel.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "CardViewModel.h"

CardViewModel::CardViewModel(const Card &card,
                             QGraphicsItem *parent)
    : QGraphicsWidget(parent)
    {
        _card = card;
        setMaximumSize(88, 128);
        setMaximumSize(88, 128);

        if(card.getCardOrientation() == Top)
        {
            _isVisible = true;
        }
        else
        {
            _isVisible = false;
        }
    }

Card CardViewModel::getCoreCard()
    {
        return _card;
    }

    void CardViewModel::setOrientation(CardOrientation orientation)
    {
        if(orientation == Top)
        {
            _isVisible = true;
        }
        else
        {
            _isVisible = false;
        }
    }

    void CardViewModel::paint(QPainter *painter,
                              const QStyleOptionGraphicsItem *,
                              QWidget *)
    {
        if (!_isVisible)
        {
           painter->drawPixmap(0, 0, 88, 128, QPixmap(":/images/card.png"));
        }
        else
        {
            stringstream ss;

            ss << ":/images/";
            ss << _card.getCardValue();
            ss << "_";
            ss << _card.getCardColor();
            ss << ".png";

            painter->drawPixmap(0, 0, 88, 128, QPixmap(ss.str().c_str()));
        }
    }

    void CardViewModel::setPosition(int x, int y)
    {
        _posX = x;
        _posY = y;

        setGeometry(QRect(x, y, x + 88, y + 128));
    }

    void CardViewModel::mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        if (_isVisible)
        {
            event->accept();

            int x = event->pos().x() + _posX;
            int y = event->pos().y() + _posY;

            if (event->button() == Qt::RightButton)
            {
                cardRightClicked(this, x, y);
            }
            else
            {
                cardSelected(this, x, y);
            }
        }
    }

    void CardViewModel::mouseMoveEvent(QGraphicsSceneMouseEvent *event )
    {
        if (_isVisible)
        {
            _moveActive = true;

            event->accept();

            int x = event->pos().x() + _posX;
            int y = event->pos().y() + _posY;

            cardMoved(this, x, y);
        }
    }

    void CardViewModel::mouseReleaseEvent(QGraphicsSceneMouseEvent *event )
    {
        if (_moveActive && _isVisible)
        {
            _moveActive = false;

            event->accept();

            int x = event->pos().x() + _posX;
            int y = event->pos().y() + _posY;

            cardReleased(this, x, y);
        }
    }
