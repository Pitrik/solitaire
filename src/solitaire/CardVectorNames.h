/**
* @file   CardVectorNames.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __CARDVECTORNAMES_
#define __CARDVECTORNAMES_

namespace core
{
    /**
     * @brief Enumeration for vectors of cards.
     */
    typedef enum
	{
		Waste,
		UsedWaste,
		Foundation1,
		Foundation2,
		Foundation3,
		Foundation4,
		Tableau1,
		Tableau2,
		Tableau3,
		Tableau4,
		Tableau5,
		Tableau6,
		Tableau7,
        Undefined
	} CardVectorNames;
}

#endif
