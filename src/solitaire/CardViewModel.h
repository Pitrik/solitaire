/**
* @file   CardViewModel.h
* @author Martin Pitrik
* @date   March, 2017
*/

#ifndef __CARD_VIEW_MODEL_
#define __CARD_VIEW_MODEL_

#include <QtCore>
#include <QtWidgets>
#include <sstream>
#include <limits>
#include "Card.h"

using namespace std;
using namespace core;

/**
 * @brief The CardViewModel class
 */
class CardViewModel : public QGraphicsWidget
{
    Q_OBJECT

public:
    /**
     * @brief Constructor for card view model.
     * @param Card core model.
     * @param Parrent, default value is 0.
     */
    CardViewModel(const Card& card, QGraphicsItem *parent = 0);

    /**
     * @brief Get core card model.
     * @return Card.
     */
    Card getCoreCard();

    /**
     * @brief Set card orientation for visibility.
     * @param CardOrientation
     */
    void setOrientation(CardOrientation orientation);

    /**
     * @brief Paint card to scene.
     * @param QPainter pointer.
     */
    void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
    Q_DECL_OVERRIDE;

    /**
     * @brief Set card position.
     * @param X position.
     * @param Y position.
     */
    void setPosition(int x, int y);

    /**
     * @brief Mouse press event handler.
     * @param QGraphicsSceneMouseEvent.
     */
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event)
    Q_DECL_OVERRIDE;

    /**
     * @brief Mouse move event handler.
     * @param QGraphicsSceneMouseEvent.
     */
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event )
    Q_DECL_OVERRIDE;

    /**
     * @brief Mouse release event handler.
     * @param QGraphicsSceneMouseEvent.
     */
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event )
    Q_DECL_OVERRIDE;

signals:
    /**
     * @brief Card selected signal.
     * @param Card view model.
     * @param X position.
     * @param Y position.
     */
    void cardSelected(CardViewModel* cardViewModel,
                      unsigned int x,
                      unsigned int y);

    /**
     * @brief Card moved signal.
     * @param Card view model
     * @param X position.
     * @param Y position.
     */
    void cardMoved(CardViewModel* cardViewModel,
                   unsigned int x,
                   unsigned int y);

    /**
     * @brief Card released signal.
     * @param Card view model
     * @param X position.
     * @param Y position.
     */
    void cardReleased(CardViewModel* cardViewModel,
                      unsigned int x,
                      unsigned int y);

    /**
     * @brief Card right clicked signal.
     * @param Card view model.
     * @param X position.
     * @param Y position.
     */
    void cardRightClicked(CardViewModel* cardViewModel,
                          unsigned int x,
                          unsigned int y);

private:
    bool _moveActive;
    int _posX;
    int _posY;
    Card _card;
    bool _isVisible;
};

#endif
