/**
* @file   Card.cpp
* @author Martin Pitrik
* @date   March, 2017
*/

#include "Card.h"

namespace core
{
    Card::Card()
    {}

    Card::Card(CardColor color, CardValue value, CardOrientation orientation)
	{
		_color = color;
		_value = value;
		_orientation = orientation;
	}

    Card::Card(const Card &card)
    {
        _color = card._color;
        _value = card._value;
        _orientation = card._orientation;
    }

	CardColor Card::getCardColor() const
	{
		return _color;
	}

	CardValue Card::getCardValue() const
	{
		return _value;
	}

	CardOrientation Card::getCardOrientation() const
	{
		return _orientation;
	}

    void Card::setCardOrientation(CardOrientation orientation)
    {
        _orientation = orientation;
    }

	void Card::changeCardOrientation()
	{
		if (_orientation == Bottom)
		{
			_orientation = Top;
		}
		else
		{
			_orientation = Bottom;
		}
	}
}
