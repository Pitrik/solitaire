export LD_LIBRARY_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/lib:${LD_LIBRARY_PATH}
export QT_PLUGIN_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/plugins:${QT_PLUGIN_PATH}

make: 
	cd src && make -f Makefile
	
clean:
	cd src && make -f Makefile clean
	rm -rf ./doc/*
	
doxygen:
	doxygen Doxyfile
	
run:
	cd src && make -f Makefile run

pack:
	make -f Makefile clean
	mkdir -p ./doc/
	rm -rf xpitri00-xsulca00-90-10.tar.gz
	tar pczvf xpitri00-xsulca00-90-10.tar.gz \
		src/ \
		doc/ \
		examples/ \
		Makefile \
		Doxyfile \
		README.txt 
	
